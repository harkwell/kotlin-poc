Gradle
=================
Build
---------------
```shell
# verify that java is installed
java -version # should be 1.8 or greater

# download and install gradle
wget -q -c 'https://services.gradle.org/distributions/gradle-7.2-bin.zip' -O /tmp/gradle.zip
sudo unzip -d /usr/local /tmp/gradle.zip
export PATH=$PATH:$(echo /usr/local/gradle*)/bin
gradle -v

# build the kotlin project here
export GRADLE_USER_HOME=/tmp/gradle-poc
gradle --no-daemon clean build
ls -ld build/libs/gradle-1.0-SNAPSHOT.jar

# execute the jar file
java -jar build/libs/gradle-1.0-SNAPSHOT.jar
```
