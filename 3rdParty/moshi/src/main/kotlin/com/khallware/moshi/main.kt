@file:JvmName("Main")

package com.khallware.moshi

//import com.squareup.moshi.KotlinJsonAdapterFactory
import com.squareup.moshi.Moshi

data class Foo(
	val id: Long,
	val key1: String,
	val key2: String
)

val json = """
{
    "key1" : "value1",
    "key2" : "value2"
}
"""

fun main(args: Array<String>)
{
	val moshi = Moshi.Builder()
//		.add(KotlinJsonAdapterFactory)
		.build()
	val adapter = moshi.adapter(Foo::class.java)
	val foo = adapter.fromJson(json)
	println(foo)
}
