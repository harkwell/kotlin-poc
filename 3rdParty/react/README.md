ReactJS and Kotlin
=================
Overview
---------------
Using kotlin and react one can create front-end applications that are nice and
maintainable.

Hello World
---------------
```shell
docker run -it --name react -h react centos
yum install -y epel-release
yum install -y java-11-openjdk less git-core
curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh |bash -
source ~/.bashrc
nvm --version
nvm install 12.13.0
npm install -g yarn
mkdir -p ~/projects/ && cd ~/projects/
yarn create react-kotlin-app khallware
cd khallware/
mkdir node_modules/.cache
yarn start

docker exec -it react bash
alias ls='ls -aCF'
alias more=less
git config --global user.email "Kevin.Hall@khallware.com"
git config --global user.name "Kevin D.Hall"
cd ~/projects/khallware/
echo .idea >>.gitignore
git init
git add .
git commit -a -m "yarn create react-kotlin-app khallware"
sed -i -e 's#Welcome#Welcome, Kevin, #' src/app/App.kt
```
