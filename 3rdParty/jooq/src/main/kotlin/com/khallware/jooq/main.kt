@file:JvmName("Main")

package com.khallware.jooq

import org.jooq.impl.DSL

// args: url user passwd
fun main(args: Array<String>)
{
	DSL.using(args[0], args[1], args[2]).use{ ctxt ->
		ctxt.resultQuery("""
			SELECT *
			FROM table1
		""")
		.fetch()
		.forEach {
			println("${it.intoMap()}")
		}
	}
}
