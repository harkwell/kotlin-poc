JOOQ (jooq object oriented querying)
=================
USAGE
---------------
```shell
docker run -it -h jooq-poc --name jooq-poc --link jooq-mariadb centos

# install build dependencies below
# build the app below

java -jar target/jooq-poc-0.1-jar-with-dependencies.jar jdbc:mysql://jooq-mariadb/foo appuser s0mep4ssw0rd
```

BUILD DEPENDENCIES
---------------
```shell
# java jdk, maven
yum install -y maven git-core
```

BUILD
---------------
```shell
git clone https://gitlab.com/harkwell/kotlin-poc.git /tmp/kotlin-poc
cd /tmp/kotlin-poc/3rdParty/jooq
export MAVEN_REPO=/tmp/kotlin-poc
mvn -Dmaven.repo.local=$MAVEN_REPO package
#rm -rf $MAVEN_REPO
```

Database
---------------
```shell
# provision an instance of mariadb
docker run -it -h jooq-mariadb --name jooq-mariadb centos
yum install -y epel-release
yum install -y mariadb mariadb-server
mysql_install_db --user=mysql --ldata=/var/lib/mysql/
/usr/bin/mysqld_safe

# connect to it and create a database
docker exec -it jooq-mariadb bash
mysql -uroot mysql
CREATE DATABASE foo;
CREATE USER 'appuser'@'%' IDENTIFIED BY 's0mep4ssw0rd';
GRANT ALL PRIVILEGES ON foo.* TO 'appuser'@'%' WITH GRANT OPTION;
USE mysql;
SET PASSWORD FOR 'appuser'@'%' = PASSWORD('s0mep4ssw0rd');
USE foo;
CREATE TABLE table1 (
	key1 VARCHAR(1024),
	key2 VARCHAR(1024)
);
INSERT INTO table1 VALUES ('value1-1','value2-1');
INSERT INTO table1 VALUES ('value1-2','value2-2');
INSERT INTO table1 VALUES ('value1-3','value2-3');
```
