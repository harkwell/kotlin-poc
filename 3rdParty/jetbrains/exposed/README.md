Maven
=================
Build
---------------
```shell
POC_MAVEN_REPO=/tmp/foo
rm -rf $POC_MAVEN_REPO
mvn -Dmaven.repo.local=$POC_MAVEN_REPO package
ls -ld target/exposed-poc-0.0.1-jar-with-dependencies.jar
java -Dlog4j.configuration=file:$PWD/log4j.properties -jar target/exposed-poc-0.0.1-jar-with-dependencies.jar
```
