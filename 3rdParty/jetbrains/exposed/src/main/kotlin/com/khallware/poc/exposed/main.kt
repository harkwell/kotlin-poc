@file:JvmName("Main")

package com.khallware.poc.exposed

import org.slf4j.LoggerFactory
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.SchemaUtils.create
import org.jetbrains.exposed.sql.SchemaUtils.drop
import java.math.BigDecimal
import java.sql.Connection
import java.nio.file.Files
import java.io.File

val DBFILE = "/tmp/exposed-poc.db"
val JDBC_URL = "jdbc:sqlite:"+DBFILE
val JDBC_DRV = "org.sqlite.JDBC"
val slf4jlog = LogAnchor().logger

class LogAnchor
{
	val logger = LoggerFactory.getLogger(LogAnchor::class.java)
}

object Recipes : Table()
{
	val id = integer("id").autoIncrement().primaryKey()
	val name = varchar("name", 50)
	val steps = varchar("steps", 2048)
}

object Items : Table()
{
	val id = integer("id").autoIncrement().primaryKey()
	val name = varchar("name", 50)
}

object RecipeItems : Table()
{
	val recipe = integer("recipe_id") references Recipes.id
	val item = integer("item_id") references Items.id
	val quantity = decimal("quantity",2,1)
	val units = text("units")
}

data class Fixin(val name: String, val quantity: BigDecimal, val units: String)
{
}

fun insertData()
{
	insertRecipe("Hot Dogs",
		"Boil or grill weiners, chop onion, heat chili, place each weiner on a bun and serve.",
		arrayOf( Fixin("chili",BigDecimal(0.5),"15oz can"),
			Fixin("hotdog buns",BigDecimal(1),"8 pack"),
			Fixin("weiners",BigDecimal(1),"8 pack"),
			Fixin("onions",BigDecimal(0.5),"medium sized")))
	insertRecipe("Hamburgers",
		"Form meat into 3 patties, grill, slice onions, shred lettuce, assemble items on a bun and serve.",
		arrayOf( Fixin("lettuce",BigDecimal(0.3),"medium head"),
			Fixin("ground beef",BigDecimal(1),"1lb pack"),
			Fixin("hamburger buns",BigDecimal(1),"8 pack"),
			Fixin("ripened tomato",BigDecimal(1),"medium"),
			Fixin("pickles",BigDecimal(0.2),"16oz jar"),
			Fixin("onions",BigDecimal(0.5),"medium sized")))
}

fun insertRecipe(recipeName: String, recipeSteps: String, fixins: Array<Fixin>)
{
	val mealId = Recipes.insert {
		it[name] = recipeName
		it[steps] = recipeSteps
	} get Recipes.id

	for (fixin in fixins) {
		// val found: Fixin? = select { Items.name.eq(fixin.name) }
		val found = Items.select { Items.name.eq(fixin.name) }
		var itemId = 0

		if (found != null) {
			val rc = Items.insert {
				it[name] = fixin.name
			} get Items.id
			itemId = rc!!
		}
		RecipeItems.insert {
			it[item] = itemId
			it[recipe] = mealId!!
			it[quantity] = fixin.quantity
			it[units] = fixin.units
		}
	}
}

/**
 * Simple PoC that creates a database for food recipes.
 *
 */
fun main(args: Array<String>)
{
	val doCreate = (!Files.exists(File(DBFILE).toPath()))
	Database.connect(JDBC_URL, driver = JDBC_DRV)
	TransactionManager.manager.defaultIsolationLevel =
		Connection.TRANSACTION_SERIALIZABLE

	transaction {
		if (doCreate) {
			slf4jlog.info("creating database {}", DBFILE)
			create (Recipes, Items, RecipeItems)
			insertData()
		}
		for (oRslts in Recipes.selectAll()) {
			// slf4jlog.trace("{}", oRslts.javaClass.getName())
			// slf4jlog.trace("{}", oRslts)
			slf4jlog.info("Recipe Id: {}", oRslts[Recipes.id])
			slf4jlog.info("Recipe Name: {}", oRslts[Recipes.name])
			slf4jlog.info("Directions: {}", oRslts[Recipes.steps])
			slf4jlog.info("Ingredients:")
	
			for (iRslts in RecipeItems.innerJoin(Items).select {
					RecipeItems.recipe eq oRslts[Recipes.id]
					}) {
				slf4jlog.info("\t{} - {} of {}",
					iRslts[RecipeItems.quantity],
					iRslts[RecipeItems.units],
					iRslts[Items.name])
			}
		}
	}
}
