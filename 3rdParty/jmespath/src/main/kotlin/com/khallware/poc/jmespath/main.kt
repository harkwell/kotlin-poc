@file:JvmName("Main")

package com.khallware.poc.jmespath

import org.slf4j.LoggerFactory
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.burt.jmespath.jackson.JacksonRuntime
import io.burt.jmespath.Expression
import io.burt.jmespath.JmesPath

class JQ
{
	val logger = LoggerFactory.getLogger(JQ::class.java)
}

val logger = JQ().logger

/**
 * java -jar target/jmespath-poc-0.1-jar-with-dependencies.jar "{}" "."
 */
fun main(args: Array<String>)
{
	logger.info("Starting...")
	var mapper = ObjectMapper().registerModule(KotlinModule())
	var jmespathRuntime = JacksonRuntime()
	var jq = jmespathRuntime.compile(args[1])
	var rslt = jq.search(mapper.readTree(args[0]))
	logger.info("${ rslt }")
	logger.info("Ending...")
}
