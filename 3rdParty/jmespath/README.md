jmespath
=================
The jmespath library ("jq" java query specification) for java
empowers the user to locate portions of json.


Build
---------------
```shell
POC_JQ_REPO=/tmp/jmespath-poc
rm -rf $POC_JQ_REPO
mvn -Dmaven.repo.local=$POC_JQ_REPO package
ls -ld target/jmespath-poc-0.1-jar-with-dependencies.jar
JSON=$(curl -s --compressed 'https://api.stackexchange.com/2.2/search?site=stackoverflow&order=desc&sort=activity&intitle=kotlin&filter=default')
#QUERY=".items[0].owner.display_name"
QUERY="items[0].owner.display_name"
java -jar target/jmespath-poc-0.1-jar-with-dependencies.jar "$JSON" "$QUERY"
```
