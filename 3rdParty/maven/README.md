Maven
=================
Build
---------------
```shell
POC_MAVEN_REPO=/tmp/foo
rm -rf $POC_MAVEN_REPO
mvn -Dmaven.repo.local=$POC_MAVEN_REPO package
ls -ld target/maven-poc-0.1-jar-with-dependencies.jar
java -jar target/maven-poc-0.1-jar-with-dependencies.jar
```
