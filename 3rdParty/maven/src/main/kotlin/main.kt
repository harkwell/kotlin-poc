@file:JvmName("Main")

package com.khallware.poc.maven

import org.slf4j.LoggerFactory

class Example
{
	val logger = LoggerFactory.getLogger(Example::class.java)
}

/**
 * kotlinc main.kt -include-runtime -d /tmp/io-poc.jar
 * java -jar /tmp/io-poc.jar
 *
 */
fun main(args: Array<String>)
{
	val logger = Example().logger
	logger.info("this is slf4j output")
}
