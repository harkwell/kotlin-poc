@file:JvmName("Main")

package com.khallware.argparser

import com.xenomachina.argparser.mainBody
import com.xenomachina.argparser.DefaultHelpFormatter
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.ArgParser.Mode

class ToolArgs(parser: ArgParser)
{
	val add by parser.flagging(
		"-a", "--add", help = "add any found items"
	)

	val force by parser.flagging(
		"-f", "--force", help = "force operation due to caution"
	)

	val recurse by parser.flagging(
		"-r", "--recurse", help = "add subdirectories as sub-tags"
	)

	val tagname by parser.storing(
		"-t", "--tag-name", help = "import under specified tagname"
	) //.default("website")

	val propfile by parser.storing(
			"-p", "--propfile", help = "khallware properties file")
		/* .default("/tmp/main.properties")
		.addValidator {
			if (!File("${ propfile) }".exists()) {
				throw RuntimeException(
					"File not found: ${ propfile }")
			}
		} */
}

/*
fun main(args: Array<String>)
{
	val parms = ArgParser(args, Mode.GNU, DefaultHelpFormatter())
		.parseInto(::ToolArgs)
	println("${ parms }")
}
*/

fun main(args: Array<String>) = mainBody {
	val parms = ArgParser(args, Mode.GNU, DefaultHelpFormatter())
		.parseInto(::ToolArgs)
	println("${ parms.tagname }")
}
