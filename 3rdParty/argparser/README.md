Argument Parsing
=================
USAGE
---------------
```shell
java -jar target/argparser-poc-0.1-jar-with-dependencies.jar --help
```

BUILD
---------------
```shell
export MAVEN_REPO=/tmp/kotlin-poc
mvn -Dmaven.repo.local=$MAVEN_REPO package
#rm -rf $MAVEN_REPO
```
