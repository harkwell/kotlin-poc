plaid.com API
=================
The plaid API is a REST-like service for processing financial transactions,
and retrieving banking information.


CONCEPTS
---------------
* An item represents a set of credentials at a financial institution.
* Creating an item will return a public_token.
* The public_token is exchanged for a temporary, thirty minute access_token.
* An access_token is required for most API endpoint interactions.


URLs
---------------
* https://github.com/plaid/plaid-java
* https://plaid.com/docs


USAGE
---------------
```shell
# https://dashboard.plaid.com/ <-- create an account, sign up for live banking
# see: dashboard -> team settings -> keys
export PLAID_ID=myplaidclientid
export PLAID_SECRET=myplaidclientsecret
export PLAID_PUBKEY=myplaidpublickey
export PLAID_ENV=dev
export WELLSFARGO_USER=user_good
export WELLSFARGO_PASS=pass_good

# test out the connection
export BODY="{ \"client_id\": \"$PLAID_ID\", \"secret\": \"$PLAID_SECRET\",
	\"count\": 10, \"offset\": 0 }"
curl -s -X POST -H "Content-Type: application/json" -d "$BODY" \
	https://sandbox.plaid.com/institutions/get |jq '.institutions[].name'
BODY="{ \"client_id\": \"$PLAID_ID\", \"secret\": \"$PLAID_SECRET\", \"query\" : \"wells\", \"products\" : [ \"transactions\" ] }"
curl -s -X POST -H "Content-Type: application/json" -d "$BODY" \
	https://sandbox.plaid.com/institutions/search |jq '.institutions[].name'

# create an item (WARNING: safely keep and secure the $PLAID_TOKEN)
sed -i -e "s#PUBLIC_KEY#$PLAID_PUBKEY#" -e "s#WF_USER#$WELLSFARGO_USER" \
	-e "s#WF_PASS#$WELLSFARGO_PASS" get-public-token.json
export PLAID_TOKEN=$(curl -s -X POST -H "Content-Type: application/json" -H "Host: sandbox.plaid.com" -d @get-public-token.json https://sandbox.plaid.com/link/item/create |jq '.public_token' |tr -d \")

# or use the sandbox one
BODY="{ \"client_id\": \"$PLAID_ID\", \"secret\": \"$PLAID_SECRET\", \"initial_products\" : [ \"transactions\" ], \"institution_id\" : \"ins_1\" }"
export PLAID_TOKEN=$(curl -s -X POST -H "Content-Type: application/json" \
	-d "$BODY" https://sandbox.plaid.com/sandbox/public_token/create \
	|jq '.public_token' |tr -d \")

for endpoint in {1..3}; do
   figlet $endpoint
   java -jar target/plaid-poc-0.1-jar-with-dependencies.jar "$endpoint"
done
```

BUILD
---------------
```shell
export MAVEN_REPO=/tmp/kotlin-poc
mvn -Dmaven.repo.local=$MAVEN_REPO package
#rm -rf $MAVEN_REPO
```
