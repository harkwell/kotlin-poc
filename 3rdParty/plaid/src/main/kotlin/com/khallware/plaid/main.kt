@file:JvmName("Main")

package com.khallware.plaid

import com.plaid.client.PlaidClient
import com.plaid.client.request.common.Product
import com.plaid.client.request.ItemPublicTokenExchangeRequest
import com.plaid.client.request.InstitutionsSearchRequest
import com.plaid.client.request.InstitutionsGetRequest
import com.plaid.client.request.TransactionsGetRequest
import java.util.Calendar
import java.util.Date

fun makePlaidClient(clientId: String, clientSecret: String,
		plaidenv: String = "dev"): PlaidClient
{
	return when (plaidenv) {
		"prod" -> PlaidClient.newBuilder()
			.clientIdAndSecret(clientId, clientSecret)
			.productionBaseUrl()
			.build()
		"int" -> PlaidClient.newBuilder()
			.clientIdAndSecret(clientId, clientSecret)
			.developmentBaseUrl()
			.build()
		else -> PlaidClient.newBuilder()
			.clientIdAndSecret(clientId, clientSecret)
			.sandboxBaseUrl()
			.build()
	}
}

fun exchangeForAccessToken(client: PlaidClient, publicToken: String): String
{
	var retval = ""
	val request = ItemPublicTokenExchangeRequest(publicToken)
	println("exchanging public token for access token...")
	retval = client.service().itemPublicTokenExchange(
		request).execute().body()!!.getAccessToken()
	return(retval)
}

fun performOne(client: PlaidClient, count: Int = 10, offset: Int = 0)
{
	val request = InstitutionsGetRequest(count,offset)
	val rslt = client.service().institutionsGet(request).execute()
	val list = rslt.body()!!.getInstitutions()
	println("found ${list.size} institutions")
	list.forEach { println("${it.getName()}") }
}

fun performTwo(client: PlaidClient, query: String = "")
{
	val request = InstitutionsSearchRequest(query)
		.withProducts(Product.TRANSACTIONS)
	val rslt = client.service().institutionsSearch(request).execute()
	val list = rslt.body()!!.getInstitutions()
	println("found ${list.size} institutions")
	list.forEach { println("${it.getName()}") }
}

fun performThree(client: PlaidClient, token: String, start: Date, end: Date)
{
	val request = TransactionsGetRequest(token, start, end)
	val rslt = client.service().transactionsGet(request).execute()
	val list = rslt.body()!!.getTransactions()
	println("found ${list.size} transactions")
	list.forEach { println("name: ${it.getTransactionId()} amount: ${
		it.getAmount()}") }
}

fun getJan1(): Date
{
	var retval = Date()
	val cal = Calendar.getInstance()
	cal.set(Calendar.DAY_OF_YEAR, 1)
	retval = cal.getTime()
	return(retval)
}

fun main(args: Array<String>)
{
	val plaidenv = System.getenv("PLAID_ENV") ?: "dev"
	val clientId = System.getenv("PLAID_ID") ?: "user_good"
	val clientSecret = System.getenv("PLAID_SECRET") ?: "pass_good"
	val clientToken = System.getenv("PLAID_TOKEN") ?: "public_token"
	println("client id: ${ clientId }, token: ${ clientToken }")
	val client = makePlaidClient(clientId, clientSecret, plaidenv)
	val routenum = args[0]!!.toInt()

	when (routenum) {
		1 -> performOne(client)
		2 -> performTwo(client, "wells")
		3 -> performThree(client,
			exchangeForAccessToken(client, clientToken),
			getJan1(), Date())
		else -> throw IllegalArgumentException(
			"unhandled endpoint: ${ routenum }")
	}
}
