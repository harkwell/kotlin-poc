@file:JvmName("Main")

package com.khallware.poc.httpclient

import org.slf4j.LoggerFactory
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.nio.charset.StandardCharsets.UTF_8
import java.time.Duration
import java.util.zip.GZIPInputStream

class HTCLNT
{
	val logger = LoggerFactory.getLogger(HTCLNT::class.java)
}

val logger = HTCLNT().logger
val API_HOST = "api.stackexchange.com"
val API_SITE = "stackoverflow"
val API_PARMS = "&order=desc&sort=activity&filter=default"

fun getSearch(token: String = "kotlin"): String
{
	var retval: String = ""
	val client = HttpClient.newBuilder()
		.connectTimeout(Duration.ofSeconds(10))
		.build()
	val request = HttpRequest.newBuilder()
		.GET()
		.uri(URI.create("https://${API_HOST
			}/2.2/search?site=${API_SITE}${API_PARMS
			}&intitle=$token"))
		.build()
	val response = client.sendAsync(request,
		HttpResponse.BodyHandlers.ofInputStream()).join()
	retval = GZIPInputStream(response.body()).bufferedReader(UTF_8)
		.use { it.readText() }
	return(retval)
}

/**
 * java -jar target/httpclient-poc-0.1-jar-th-dependencies.jar java
 */
fun main(args: Array<String>)
{
	logger.info("${ args }\"")
	logger.info("${ getSearch(args[0]) }\"")
}
