HttpClient (java11)
=================
This is how to call java.net.http.HttpClient asynchronously.


Build
---------------
```shell
POC_HTTP_REPO=/tmp/httpclient-poc
rm -rf $POC_HTTP_REPO
mvn -Dmaven.repo.local=$POC_HTTP_REPO package
ls -ld target/httpclient-poc-0.1-jar-with-dependencies.jar
java -jar target/httpclient-poc-0.1-jar-with-dependencies.jar foo
chromium-browser http://localhost:8080/v1/apis/login.html
```
