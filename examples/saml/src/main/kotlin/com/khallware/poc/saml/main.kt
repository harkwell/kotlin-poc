@file:JvmName("Main")

package com.khallware.poc.saml

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import javax.servlet.http.HttpServletRequest
import javax.ws.rs.core.Response
import javax.ws.rs.core.Context
import javax.ws.rs.Produces
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import java.net.URI
import java.util.Set
import java.util.HashSet
import org.slf4j.LoggerFactory
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.servlet.ServletHolder
import org.eclipse.jetty.servlet.ServletContextHandler
import org.glassfish.jersey.servlet.ServletContainer

class SAML
{
	val logger = LoggerFactory.getLogger(SAML::class.java)
}

val logger = SAML().logger
val HDR_CC = "private, max-age=0, no-cache"
val SITE_URL = "http://localhost:8080"
val LOGIN_URL = "${ SITE_URL }/v1/apis/login.html"
val INDEX_URL = "${ SITE_URL }/v1/apis/index.html"
val AUTHCD_URL = "${ SITE_URL }/v1/apis/storeauthcode"
val CLIENT_SECRET = "YOUR_CLIENT_SECRET"
var authcode = ""
var accesstoken = ""

@Path("/v1/apis")
class Ctrl
{
	@GET
	@Path("/index.html")
	@Produces("text/html")
	fun getIndex() : Response
	{
		var retval = Response.status(200).build()
		logger.warn("let's protect this!")

		if (authcode.isEmpty()) {
			logger.info("redirecting to login.html")
			retval = Response.status(303)
				.header("Location", LOGIN_URL)
				.header("Cache-Control",HDR_CC)
				.build()
		}
		else {
			logger.info("redirecting to index.html")
			retval = Response.status(200)
				.header("Cache-Control",HDR_CC)
				.entity(Content().getProtectedPage())
				.build()
		}
		return(retval)
	}

	/**
	 * https://developers.google.com/identity/sign-in/web/server-side-flow
	 */
	@GET
	@Path("/login.html")
	fun getLogin() : String
	{
		return(Content().getLoginPage())
	}

	@POST
	@Path("/storeauthcode")
	fun post(@Context request: HttpServletRequest, body: String) : Response
	{
		var retval = Response.status(200).build()
		logger.info("Google authcode is: ${ body }")

		if (request.getHeader("X-Requested-With") == null) {
			val msg = "request may be forged!"
			logger.error(msg)
			retval = Response.status(200)
				.header("Cache-Control",HDR_CC)
				.entity(msg)
				.build()
		}
		else {
			authcode = body

			if (!exchangeAuthcodeForAccessToken()) {
				retval = Response.status(303)
					.header("Location", LOGIN_URL)
					.header("Cache-Control",HDR_CC)
					.build()
			}
			else {
				retval = Response.status(301)
					.header("Location", INDEX_URL)
					.header("Cache-Control",HDR_CC)
					.build()
			}
		}
		return(retval)
	}

	/*
	@GET
	@Path("/storeauthcode")
	fun getAC(@Context request: HttpServletRequest, body: String) : Response
	{
		logger.error("don't call me...")
		return(Response.status(303)
			.header("Location", LOGIN_URL)
			.header("Cache-Control",HDR_CC)
			.build())
	}
	*/
}

fun exchangeAuthcodeForAccessToken() : Boolean
{
	var retval = false
	try {
		val response = GoogleAuthorizationCodeTokenRequest(
			NetHttpTransport(), JacksonFactory.getDefaultInstance(), 
			"https://www.googleapis.com/oauth2/v4/token",
			"YOUR_CLIENT_ID.apps.googleusercontent.com",
			CLIENT_SECRET, authcode, SITE_URL).execute()
		val payload = response.parseIdToken().getPayload()
		accesstoken = response.getAccessToken()
		logger.info("Username: ${ payload.getSubject() }")
		logger.info("Email: ${ payload.getEmail() }")
		logger.info("Email-verfied?: ${ payload.getEmailVerified() }")
		logger.info("Name: ${ payload.get("name") }")
		logger.info("Picture-URL: ${ payload.get("picture") }")
		logger.info("Locale: ${ payload.get("locale") }")
		logger.info("Family-Name: ${ payload.get("family_name") }")
		logger.info("Given-Name: ${ payload.get("given_name") }")
		logger.trace("${ payload }")
	}
	catch (e: Exception) {
		logger.error("${ e }", e)
	}
	retval = (!accesstoken.isEmpty())
	return(retval)
}

fun initJetty(url: String = "${ SITE_URL }") : Server
{
	var retval = Server(8080)  // use url
	val ctxt = ServletContextHandler(ServletContextHandler.SESSIONS)
	ctxt.setContextPath("/")
	retval.setHandler(ctxt)
	val servletHolder = ctxt.addServlet(ServletContainer::class.java, "/*")
	servletHolder.setInitOrder(0)
	servletHolder.setInitParameter(
		"jersey.config.server.provider.classnames",
		Ctrl::class.java.getCanonicalName())
	return(retval)
}

/**
 * java -jar target/saml-poc-0.1-jar-th-dependencies.jar foo
 * curl http://localhost:8080/v1/apis/index.html
 */
fun main(args: Array<String>)
{
	val svr = initJetty(args[0])
	logger.info("${ args }\"")
	svr.start()
	svr.join()
	// svr.destroy()
}
