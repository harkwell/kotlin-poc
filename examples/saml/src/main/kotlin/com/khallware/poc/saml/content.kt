package com.khallware.poc.saml

class Content
{
	fun getProtectedPage() : String
	{
		return("""
<html>
<body>
This is protected content...
</body>
</html>
		""")
	}

	fun getLoginPage() : String
	{
		return("""
<!-- The top of file index.html -->
<html itemscope itemtype="http://schema.org/Article">
<head>
  <!-- BEGIN Pre-requisites -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js">
  </script>
  <script src="https://apis.google.com/js/client:platform.js?onload=start" async defer>
  </script>
  <!-- END Pre-requisites -->

  <script>
    function start() {
      gapi.load('auth2', function() {
        auth2 = gapi.auth2.init({
          client_id: '1087718054556-7ppc1cf07nrl0rejdgakv3b6184ba2lf.apps.googleusercontent.com',
          // Scopes to request in addition to 'profile' and 'email'
          //scope: 'additional_scope'
        });
      });
    }
  </script>
</head>
<body>
  <h1>Login via SAML</h1>
<button id="signinButton">Sign in with Google</button>
<script>
  $('#signinButton').click(function() {
    auth2.grantOfflineAccess().then(signInCallback);
  });
</script>
<script>
function signInCallback(authResult) {
  if (authResult['code']) {

    // Hide the sign-in button now that the user is authorized, for example:
    $('#signinButton').attr('style', 'display: none');

    // Send the code to the server
    $.ajax({
      type: 'POST',
      url: '${ AUTHCD_URL }',
      // Always include an `X-Requested-With` header in every AJAX request,
      // to protect against CSRF attacks.
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      },
      contentType: 'application/octet-stream; charset=utf-8',
      success: function(data, status, result) {
        console.log(result)
        // debugger
        // window.location.replace(result.getResponseHeader('Location'))
        window.location.replace("${ INDEX_URL }")
      },
      processData: false,
      data: authResult['code']
    });
  } else {
    // There was an error.
    console.log("error!")
  }
}
</script>
</body>
</html>
		""")
	}
}
