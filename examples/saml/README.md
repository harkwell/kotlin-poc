SAML (Security Assertion Markup Language)
=================
An XML based format, protocol and algorithm to exchange authentication and
authorization information between components.  This is used quite a bit for
single-sign-on functionality.


Build
---------------
```shell
POC_SAML_REPO=/tmp/saml-poc
rm -rf $POC_SAML_REPO
sed -i -e 's#YOUR_CLIENT_ID#'$CLIENT_ID'#g' src/main/kotlin/com/khallware/poc/saml/main.kt
sed -i -e 's#YOUR_CLIENT_ID#'$CLIENT_ID'#g' src/main/kotlin/com/khallware/poc/saml/content.kt
sed -i -e 's#YOUR_CLIENT_SECRET#'$CLIENT_SECRET'#g' src/main/kotlin/com/khallware/poc/saml/main.kt
mvn -Dmaven.repo.local=$POC_SAML_REPO package
ls -ld target/saml-poc-0.1-jar-with-dependencies.jar
java -jar target/saml-poc-0.1-jar-with-dependencies.jar foo
chromium-browser http://localhost:8080/v1/apis/login.html
```

One-Time Setup
---------------
```shell
chromium-browser https://console.developers.google.com/
# API -> Credentials -> Create cred... -> OAuth client ID
#     -> Configure consent -> Web app... -> origins -> "http://localhost:8080"
#     -> redirects -> "http://localhost:8080/v1/apis/storeauthcode"
#     -> Create
#     save: "client ID" and "client secret"
chromium-browser https://console.developers.google.com/project/_/apiui/apis/library
export CLIENT_ID=<some long hash string>
export CLIENT_SECRET=<some shorter hash string>
```

Configure
---------------
```shell
# create an application id from your identity provider (IdP)
chromium-browser https://developers.google.com/identity/sign-in/web/server-side-flow
chromium-browser https://myaccount.google.com/permissions
```

Documentation
---------------
* https://developers.google.com/identity/
* https://support.google.com/a/answer/6087519
