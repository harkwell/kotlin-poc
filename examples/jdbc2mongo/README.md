jdbc2mongo (relational database to schema-less)
=================
USAGE
---------------
```shell
docker run -it -h jdbc2mongo-poc --name jdbc2mongo-poc --link jdbc2mongo-mariadb --link jdbc2mongo-mongodb centos

# install build dependencies below
# build the app below

java -jar target/jdbc2mongo-poc-0.1-jar-with-dependencies.jar jdbc:mysql://jdbc2mongo-mariadb appuser s0mep4ssw0rd jdbc2mongo-mongodb

# test
docker exec -it jdbc2mongo-mongodb bash
mongo mongodb://localhost
use etl
db.etldata.find()
```

BUILD DEPENDENCIES
---------------
```shell
# java jdk, maven
yum install -y maven git-core
```

BUILD
---------------
```shell
git clone https://gitlab.com/harkwell/kotlin-poc.git /tmp/kotlin-poc
cd /tmp/kotlin-poc/examples/jdbc2mongo
export MAVEN_REPO=/tmp/kotlin-poc-repo
mvn -Dmaven.repo.local=$MAVEN_REPO package
#rm -rf $MAVEN_REPO
```

Database
---------------
```shell
# provision an instance of mariadb
docker run -it -h jdbc2mongo-mariadb --name jdbc2mongo-mariadb centos
yum install -y epel-release
yum install -y mariadb mariadb-server
mysql_install_db --user=mysql --ldata=/var/lib/mysql/
/usr/bin/mysqld_safe

# connect to it and create a database
docker exec -it jdbc2mongo-mariadb bash
mysql -uroot mysql
CREATE DATABASE foo1;
CREATE USER 'appuser'@'%' IDENTIFIED BY 's0mep4ssw0rd';
GRANT ALL PRIVILEGES ON foo1.* TO 'appuser'@'%' WITH GRANT OPTION;
USE mysql;
SET PASSWORD FOR 'appuser'@'%' = PASSWORD('s0mep4ssw0rd');
USE foo1;
CREATE TABLE table1 (
	foo1key1 VARCHAR(1024),
	foo1key2 VARCHAR(1024)
);
INSERT INTO table1 VALUES ('foo1-value1-1','foo1-value2-1');
INSERT INTO table1 VALUES ('foo1-value1-2','foo1-value2-2');
INSERT INTO table1 VALUES ('foo1-value1-3','foo1-value2-3');

CREATE DATABASE foo2;
CREATE USER 'appuser'@'%' IDENTIFIED BY 's0mep4ssw0rd';
GRANT ALL PRIVILEGES ON foo2.* TO 'appuser'@'%' WITH GRANT OPTION;
USE mysql;
SET PASSWORD FOR 'appuser'@'%' = PASSWORD('s0mep4ssw0rd');
USE foo2;
CREATE TABLE table1 (
	foo2key1 VARCHAR(1024),
	foo2key2 VARCHAR(1024)
);
INSERT INTO table1 VALUES ('foo2-value1-1','foo2-value2-1');
INSERT INTO table1 VALUES ('foo2-value1-2','foo2-value2-2');
INSERT INTO table1 VALUES ('foo2-value1-3','foo2-value2-3');


# provision an instance of mongodb
docker run -it -h jdbc2mongo-mongodb --name jdbc2mongo-mongodb centos
cat <<'EOF' >/etc/yum.repos.d/mongodb-org.repo
[mongodb-org-3.4]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/3.4/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-3.4.asc
EOF
yum install -y mongodb-org
sed -in -e 's#bindIp:.*$#bindIp: 0.0.0.0#' /etc/mongod.conf
/usr/bin/mongod -f /etc/mongod.conf
tail -f /var/log/mongodb/mongod.log
```
