@file:JvmName("Main")

package com.khallware.jdbc2mongo

import java.sql.DriverManager
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.mongodb.DBCollection
import com.mongodb.BasicDBObject
import com.mongodb.MongoClient
import com.mongodb.DB
import org.jooq.impl.DSL
import org.jooq.Record

fun etl(jdbc: String, username: String, passwd: String, mongohost: String)
{
	val client = MongoClient(mongohost, 27017)

	listDatabases(jdbc, username, passwd).forEach() {
		println("Extracting data from database: ${ it }")
		extract("${jdbc}/${it}", username, passwd).forEach() { x ->
			load(client, translate(x))
			println("${x.toJsonString()}")
		}
	}
}

fun listDatabases(url: String, username: String, passwd: String): List<String>
{
	val retval : MutableList<String> = arrayListOf()
	val connection = DriverManager.getConnection(url, username, passwd)
	val metadata = connection.getMetaData()
	val resultSet = metadata.getCatalogs()

	while (resultSet.next()) {
		val dbname = resultSet.getString("TABLE_CAT")

		when (dbname) { 
			"information_schema" -> ""
			"performance_schema" -> ""
			"mysql" -> ""
			"test" -> ""
			else -> retval.add(resultSet.getString("TABLE_CAT"))
		}
	}
	resultSet.close();
	connection.close();
	return(retval)
}

fun resultToJson(record: Record): String
{
	var retval = StringBuilder()

	record.fields().forEach {
		val recordKey = it.getName()
		val recordValue = record.getValue(0,it)

		retval.append(if (retval.count() == 0) "{ " else ", ")
		retval.append("\"${ recordKey }\" : \"${ recordValue }\"")
	}
	// retval = StringBuilder("${ retval.dropLast(2) }")
	retval.append(" }")
	return("${ retval }")
}

fun extract(url: String, username: String, passwd: String): List<JsonObject>
{
	val retval : MutableList<JsonObject> = arrayListOf()
	val parser: Parser = Parser()
	val json = DSL.using(url, username, passwd).use{ ctxt ->
		ctxt.resultQuery("""
			SELECT *, 'mydbname' AS dbname
			FROM table1
		""")
		.fetch { resultToJson(it) }
	}
	println("found ${json}")
	val sb = StringBuilder("{ \"items\" : ${json} }")
	val obj = parser.parse(sb) as JsonObject
	retval.add(obj)
	return(retval)
}

fun translate(retval: JsonObject): JsonObject
{
	return(retval)
}

fun load(client: MongoClient, json: JsonObject)
{
	val db = client.getDatabase("etl")
	val colname = "etldata"
	val collection = db.getCollection(colname, BasicDBObject::class.java)
	collection.insertOne(BasicDBObject.parse(json.toJsonString()))
}

// args: jdbc_url user passwd mongo_host
fun main(args: Array<String>)
{
	etl(args[0], args[1], args[2], args[3])
}
