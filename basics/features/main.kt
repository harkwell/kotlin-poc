@file:JvmName("Main")

package com.khallware.poc.kotlin

/**
 * kotlinc main.kt -include-runtime -d /tmp/kotlin-poc.jar
 * java -jar /tmp/kotlin-poc.jar
 *
 */
fun functionAsParameter(visitor: (content: String) -> Unit)
{
	val part = mutableListOf("wing","foot","beard")
	val action = mutableListOf("eat","kick","stomp","catapult")
	val animal = mutableListOf("turkey","chicken","goat")
	val adjective = mutableListOf("rotten","sweetened","golden-brown")
	visitor("""
		May you ${action.shuffled().get(0)} 
		the ${part.shuffled().get(0)} 
		of a ${adjective.shuffled().get(0)} ${animal.shuffled().get(0)}.
		""".trimIndent().replace("\n",""))
}

// default and named function parms
fun functionParms(parm1: String, parm2: Int, parm3: Boolean = true) : Boolean
{
	println("named parm parm1 is ${parm1} and default parm3 is ${parm3}")
	return(!parm3)
}

// extension function for java.lang.String
fun String.rot13(): String
{
	var retval = ""

	for (c in this) {
		when (c) {
			in 'a'..'m', in 'A'..'M' -> retval += (c + 13).toChar()
			in 'n'..'z', in 'N'..'Z' -> retval += (c - 13).toChar()
			else -> retval += c
		}
	}
	return(retval)
}

// infix functions extend classes and look like operators
infix fun String.sumAsInt(term: String) : String
{
	val retval = this.toInt() + term.toInt()
	return("${retval}")
}

data class Monocle(val side: String)
{
	operator fun plus(m: Monocle): Glasses
	{
		return(Glasses(this, m))
	}
}

data class Glasses(val m1: Monocle, val m2: Monocle)
{
}

tailrec fun <T: Comparable<T>> bubbleSort(items: MutableList<T>, i: Int)
{
	if (i != 1) {
		for (idx in 0..i-1) {
			if (items[idx] > items[idx+1]) {
				val tmp = items[idx]
				items[idx] = items[idx+1]
				items[idx+1] = tmp
			}
		}
		bubbleSort(items, i-1)
	}
}

interface I1
{
	fun M1()
	{
	}
}

interface I2
{
	fun M1()
	{
	}
}

class MultipleInheritance : I1, I2
{
	override fun M1()
	{
		super<I1>.M1()
	}
}

sealed class Coin
{
	class Penny : Coin()
	class Nickel : Coin()
	class Dime : Coin()
	class Quarter : Coin()
	class Dollar : Coin()
}

fun handle(coin: Coin, visitor: (amount: Int) -> Unit)
{
	when (coin) {
		is Coin.Penny -> visitor(1)
		is Coin.Nickel -> visitor(5)
		is Coin.Dime -> visitor(10)
		is Coin.Quarter -> visitor(25)
		is Coin.Dollar -> visitor(100)
	}
}

open class Shoe(val brand: String)
{
}

class BasketballShoe : Shoe
{
	constructor(brand: String) : super(brand)

	// our "singleton's" scope is limited to the Shoe class
	object BrandComparator : Comparator<Shoe>
	{
		override fun compare(shoe1: Shoe, shoe2: Shoe) : Int
		{
			return(shoe1.brand.toString().compareTo(shoe2.brand))
		}
	}
}

class Item
{
	var name: String = ""
	var price = 0.00
	var number =  0

	fun tax(rate: Double)
	{
		price += (rate * price)
	}
}

data class Widget(val id: Int, val name: String)
{
	var price = 0.00
}

fun showcaseCollections()
{
	val items = listOf(Widget(1,"Right Wing"), Widget(2,"Left Wing"),
		Widget(3,"Rudder"), Widget(4,"Propeller"), Widget(5,"Body"))

	for (item in items
			.filter {println("filter $it"); it.name.contains("ing")}
			.map {println("map $it"); it.name}) {
		println(item)
	}
	println("now as a sequence...")

	for (item in items
			.asSequence()
			.filter {println("filter $it"); it.name.contains("ing")}
			.map {println("map $it"); it.name}) {
		println(item)
	}
}

fun showcaseNPE()
{
	var v1: String = ""  // v1 cannot be null
	// v1 = null         // results in a compiler error

	var v2: String? = "" // v2 can be null
	v2 = null

	var v3: Int? = null
	v3 = v2?.length      // if v2 is not null, set v3 to len else null
	val v4 = v3 ?: 0     // if v3 is not null, set v4 to len else 0

	var v5: String? = ""
	val v6 = v5!!.length // throw NPE if v5 is null, else set v6 to len

	val list1: List<Int?> = listOf(0,5,null,15,20)
	val list2: List<Int> = list1.filterNotNull()
	println("$v1 $v2 $v3 $v4 $v5 $v6 $list1 $list2")
}

fun main(args: Array<String>)
{
	functionAsParameter(::println)
	functionParms(parm2 = 1, parm1 = "foo")
	val content = "extension functions in rot13"
	println("${content} = ${content.rot13()}")

	val seven = "7"
	val three = "3"
	println("7 and 3 is ${seven sumAsInt three}")

	val left = Monocle("left")
	val right = Monocle("right")
	println("${left + right}") // operator overloading

	// tail recursive functions
	val size = 10000
	val items = listOf(0..(size-1)).flatten().shuffled().toMutableList()
	// val spent = measureTimeMillis { bubbleSort(items, size-1) }
	bubbleSort(items, size-1)
	// println("${size} elements took ${spent} milliseconds to bubble sort")
	println("first 10 sorted: ${items.subList(0,10)}")

	// multiple inheritance with duplicate method signatures
	val mi = MultipleInheritance()

	// sealed classes (like enums)
	// high level functions (strategy pattern)
	var sum = 0

	for (c in arrayOf(Coin.Penny(), Coin.Penny(), Coin.Quarter(),
			Coin.Quarter(), Coin.Dollar())) {
		handle(c, { a -> sum += a })
	}
	println("array of coins sums to \$${sum.toFloat() / 100}")

	// secondary constructors
	BasketballShoe("Adidas")

	// data classes
	val m = Monocle("left-outer")
	println("${m}")

	// companion objects (singleton)
	var msg = StringBuilder().append("Adidas and Nike are ")
	msg.append(if (Shoe("Adidas") == Shoe("Nike")) "" else "not ")
	println("${msg.toString()}the same shoe")

	// "with" construct and "apply" (for builder)
	val item1 = Item()
	val item2 = Item()

	with (item1) {
		name = "my-first-item"
		price = 10.25
		number = 5
	}
	item2.apply {
		name = "my-second-item"
		price = 12.99
		number = 8
	}.tax(.06)

	// collections and infinite collections (sequences)
	showcaseCollections()

	// null pointer handling
	showcaseNPE()

	// arrays
	// generics
	// variance (in and out keywords)
}
