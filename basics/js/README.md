Kotlin Javascript
=================
Build
---------------
```shell
POC_MAVEN_REPO=/tmp/foo
rm -rf $POC_MAVEN_REPO
mvn -Dmaven.repo.local=$POC_MAVEN_REPO compile
ls -ld target/js/kotlin-js-poc.js
unzip $POC_MAVEN_REPO/org/jetbrains/kotlin/kotlin-stdlib-js/1.2.30/kotlin-stdlib-js-1.2.30.jar kotlin.js
chromium-browser target/classes/index.html
```
