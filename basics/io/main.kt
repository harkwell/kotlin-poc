@file:JvmName("Main")

package com.khallware.poc.io

import java.io.File

/**
 * kotlinc main.kt -include-runtime -d /tmp/io-poc.jar
 * java -jar /tmp/io-poc.jar
 *
 */
fun perline(fname: String)
{
	var count = 0

	File(fname).forEachLine {
		val dat = it.split(":")
		println("user-${++count} (id=${dat[2]}) = "
			+"(${dat[0]}) \"${dat[4].split(",")[0]}\"")
	}
}

fun readContents(fname: String): String
{
	var count = 0
	val contents = File(fname).readLines()
	contents.forEach { println("line-${++count} has ${it.length} bytes") }

	return(String(File(fname).readBytes()))
}

fun recurse(dirname: String)
{
	val list = ArrayList<String>()

	File(dirname).walkTopDown().forEach {
		if (it.isDirectory()) {
			list.add(it.name)
		}
	}
	println(list)
}

fun writeContents(fname: String, contents: String)
{
	File(fname).printWriter().use { out -> { out.println(contents) } }
}

fun main(args: Array<String>)
{
	val fname = "/etc/passwd"
	perline(fname)
	println("file ${fname} has ${readContents(fname).length} bytes")
	recurse("/dev/")
	writeContents("/dev/null", (0..9)
		.map { "line-${it}\n" }
		.fold("", { l,r -> "${l} ${r}" }))
}
