@file:JvmName("Main")

package com.khallware.poc.constructs

/**
 * kotlinc main.kt -include-runtime -d /tmp/constructs-poc.jar
 * java -jar /tmp/constructs-poc.jar
 *
 */
fun conditionals()
{
	println("kotlin conditionals:")
	println("\tif statement")
	println("\ttry-catch-finally statement")
	println("\twhen statement")

	if (false) {
	}
	else if (false) {
	}
	else {
	}
	try {
		throw RuntimeException("foo")
	}
	catch (e: Throwable) {
	}
	finally {
	}
	var x = if (true) 1 else 0
	val y = when (x) {
		0 -> "zero"
		1 -> "one"
		2 -> "two"
		else -> "unknown"
	}
	println(y)
}

fun looping()
{
	println("kotlin looping:")
	println("\tfor statement")
	println("\twhile statement")
	println("\tdo-while statement")

	MYLOOP@ for (i in 0..100) {
		break@MYLOOP
	}
	while (true) {
		break
	}
	var x = if (true) 1 else 0
	do {
		if (x > 1) break
		x++
	} while (true)

	val y = when (x) {
		0 -> "zero"
		1 -> "one"
		2 -> "two"
		else -> "unknown"
	}
	println(y)
}

fun returnAValue(): String
{
	return("method values are sent with the return() statement")
}

fun main(args: Array<String>)
{
	conditionals()
	looping()
	println(returnAValue())
}
