@file:JvmName("Main")

package com.khallware.virus

import com.khallware.virus.Nucleobase.*

/**
 * kotlinc main.kt -include-runtime -d /tmp/flu-virus-poc.jar
 * java -jar /tmp/flu-virus-poc.jar
 *
 */

const val RIBOSE = "ribose"

enum class Nucleobase(val molecule: String)
{
	A("adenine"),
	C("cytosine"),
	G("guanine"),
	T("thymine"),
	U("uracil")
}

/*
 * A Nucleotide is the building block for RNA and DNA in the cell and is
 * used for metabolism, cell signaling and division and carries energy.
 */
data class Nucleotide(val sugar: String, val base: Nucleobase,
		val phosphate: String)
{
}

/*
 * RNA (Ribonucleic acid) carries the genetic instructions for cell behavior.
 */
interface RNA
{
	fun getNucleotides(): Array<Nucleotide>
}

/*
 * A virus is a small infectious agent that replicates only inside the living
 * cells of other organisms.
 */
interface Virus
{
	fun replicate(): Virus
	fun impact(): Array<String>
}

open class Flu: RNA, Virus
{
	val a = Nucleotide(RIBOSE, A, "PO4")
	val c = Nucleotide(RIBOSE, C, "PO4")
	val g = Nucleotide(RIBOSE, G, "PO4")
	val t = Nucleotide(RIBOSE, T, "PO4")
	val u = Nucleotide(RIBOSE, U, "PO4")

	fun getStrain(): String
	{
		return(this.javaClass.getSimpleName())
	}

	override fun getNucleotides(): Array<Nucleotide>
	{
		return(arrayOf())
	}
	override fun replicate(): Virus
	{
		return(this)
	}
	override fun impact(): Array<String>
	{
		return(arrayOf())
	}
}

open class FluA: Flu()
{
	override fun getNucleotides(): Array<Nucleotide>
	{
		return(arrayOf(
			a, c, a, a, a, g, a, t, u, c, u, u, a, c, u, a, t, g,
			g, a, t, u, c, a, a, a, a, g, a, t, c, a, a, a, g, u,
			c, a, a, a, u, c, a, a, g, a, u, a, t, g, t, u, c, a,
			t, u, c, u, g, a, t, u, c, u, a, t, a, u, c, a, a, t,
			g, a, t, u, c, a, a, a, a, g, a, t, c, a, a, a, g, u,
			c, a, a, a, u, c, a, a, g, a, u, a, t, g, t, u, c, a,
			a, c, a, a, a, g, a, t, u, c, u, u, a, c, u, a, t, g,
			a
		))
	}
}

class FluB: Flu()
{
	override fun getNucleotides(): Array<Nucleotide>
	{
		return(arrayOf(
			g, a, t, u, c, a, a, a, a, g, a, t, c, a, a, a, g, u,
			a, c, a, a, a, g, a, t, u, c, u, u, a, c, u, a, t, g,
			c, a, a, a, u, c, a, a, g, a, u, a, t, g, t, u, c, a,
			g, a, t, u, c, a, a, a, a, g, a, t, c, a, a, a, g, u,
			t, u, c, u, g, a, t, u, c, u, a, t, a, u, c, a, a, t,
			a, c, a, a, a, g, a, t, u, c, u, u, a, c, u, a, t, g,
			c, a, a, a, u, c, a, a, g, a, u, a, t, g, t, u, c, a,
			a
		))
	}
	override fun impact(): Array<String>
	{
		return(arrayOf(
			"less common than influenza A.",
			"mutates at a rate 2-3 times slower than type A",
			"a degree of immunity is acquired at an early age"
		))
	}
}

class FluC: Flu()
{
	override fun getNucleotides(): Array<Nucleotide>
	{
		return(arrayOf(
			a, c, a, a, a, g, a, t, u, c, u, u, a, c, u, a, t, g,
			g, a, t, u, c, a, a, a, a, g, a, t, c, a, a, a, g, u,
			t, u, c, u, g, a, t, u, c, u, a, t, a, u, c, a, a, t,
			c, a, a, a, u, c, a, a, g, a, u, a, t, g, t, u, c, a,
			c, a, a, a, u, c, a, a, g, a, u, a, t, g, t, u, c, a,
			g, a, t, u, c, a, a, a, a, g, a, t, c, a, a, a, g, u,
			a
		))
	}
	override fun impact(): Array<String>
	{
		return(arrayOf(
			"infects humans, dogs and pigs",
			"causes mild disease in children"
		))
	}
}

class FluD: Flu()
{
	override fun getNucleotides(): Array<Nucleotide>
	{
		return(arrayOf(
			t, u, c, u, g, a, t, u, c, u, a, t, a, u, c, a, a, t,
			a, c, a, a, a, g, a, t, u, c, u, u, a, c, u, a, t, g,
			g, a, t, u, c, a, a, a, a, g, a, t, c, a, a, a, g, u,
			c, a, a, a, u, c, a, a, g, a, u, a, t, g, t, u, c, a,
			g, a, t, u, c, a, a, a, a, g, a, t, c, a, a, a, g, u,
			c, a, a, a, u, c, a, a, g, a, u, a, t, g, t, u, c, a,
			a
		))
	}
}

class H1N1: FluA()
{
	override fun impact(): Array<String>
	{
		return(arrayOf(
			"Spanish Flu of 1918",
			"Swine Flu of 2009"
		))
	}
}

class H2N2: FluA()
{
	override fun impact(): Array<String>
	{
		return(arrayOf(
			"Asian Flu of 1957"
		))
	}
}

class H3N2: FluA()
{
	override fun impact(): Array<String>
	{
		return(arrayOf(
			"Hong Kong Flu of 1968"
		))
	}
}

class H5N1: FluA()
{
	override fun impact(): Array<String>
	{
		return(arrayOf(
			"Bird Flu of 2004"
		))
	}
}

class H7N7: FluA()
{
	override fun impact(): Array<String>
	{
		return(arrayOf(
			"has unusual zoonotic potential"
		))
	}
}

class H1N2: FluA()
{
	override fun impact(): Array<String>
	{
		return(arrayOf(
			"endemic in humans, pigs and birds"
		))
	}
}

fun main(args: Array<String>)
{
	val list = arrayOf(
		H1N1(), H2N2(), H3N2(), H5N1(), H7N7(), H1N2(), FluB(), FluC())

	for (flu in list) {
		val strain = flu.getStrain()
		println("influenza strain $strain")

		for (i in flu.impact()) {
			println("\t$i")
		}
	}
}
