Khallware (Kotlin OOD/OOP Proofs-of-Concept)
=================
Overview
---------------
Object Oriented Design (OOD) is a software engineering paradigm that combines
data and methods to pattern real-world problems and concepts.  Object Oriented
Programming (OOP) constitutes how things in the OOD paradigm play together.
It's the way that objects communicate versus object dependencies in OOD.
In this case kotlin is the OOP platform.  OOD can be reflected in UML or other
diagramming tools.

In this PoC, we model the influenza virus strains A through D.
