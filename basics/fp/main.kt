@file:JvmName("Main")

package com.khallware.poc.fp

/**
 * kotlinc main.kt -include-runtime -d /tmp/fp-poc.jar
 * java -jar /tmp/fp-poc.jar
 *
 */
fun immutableTypes()
{
	val myval = "foo"  // the variable myval cannot be changed
	// myval = "bar"   // it's immutable (results in a compile-time error)
}

fun stringInterpolation()
{
	val myval = "string interpolation"
	println("this is ${myval}")
}

fun ranges()
{
	println("we can leverage ranges easily...")
	(0..5).forEach { println("this is range value ${it}") }
}

fun main(args: Array<String>)
{
	immutableTypes()
	stringInterpolation()
	ranges()
}
