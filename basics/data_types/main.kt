@file:JvmName("Main")

package com.khallware.poc.datatypes

/**
 * kotlinc main.kt -include-runtime -d /tmp/datatypes-poc.jar
 * java -jar /tmp/datatypes-poc.jar
 *
 */
fun numbers()
{
	println("Long = 64 bits (eg 1_024L)")
	println("Double = 64 bits (eg 10.24e100)")
	println("Int = 32 bits (eg 1024)")
	println("Float = 32 bits (eg 99.99f)")
	println("Short = 16 bits (eg 1024.toShort())")
	println("Byte = 16 bits (eg 0b10101101)")
}

fun characters()
{
	val content = "for turning into rot13"
	var rot13 = ""

	for (c in content) {
		when (c) {
			in 'a'..'m', in 'A'..'M' -> rot13 += (c + 13).toChar()
			in 'n'..'z', in 'N'..'Z' -> rot13 += (c - 13).toChar()
			else -> rot13 += c
		}
	}
	println("string '${content}' is '${rot13}' in rot13")
}

fun arrays()
{
	val array1 = arrayOf( 0, 5, 3, 2, 9, 55 )
	println("One dimensional array (eg ${ array1.contentDeepToString() })")
	var cnt = 0
	val array2 = Array<Array<String>>(3) {
		Array<String>(3) { "row-${cnt / 3 + 1}-col-${cnt++ % 3 + 1}" }
	}
	println("Multi-dimensional array (example):")

	for (row in array2) {
		println(row.contentDeepToString())
	}
	array1.sort()
	println("Sorted array (eg \"${ array1.contentDeepToString() }\")")
	println("Access row-2-col-3 (\"${ array2[1][2] }\")")
}

fun enums()
{
	for (e in enumValues<Character.UnicodeScript>()) {
		if (e == Character.UnicodeScript.CHEROKEE) {
			println("Found enum \"${ e }\"")
		}
	}
}

interface MyInterface
{
	fun execute(arg: String)
	{
		// default implementation
		throw RuntimeException("not yet implemented")
	}
}

class Foo(myval: Int)
{
	val content = "my foo has value, ${myval}";

	init
	{
		println("this is like java static blocks")
	}

	fun method1()
	{
		println("method1() called")
		println("content: ${content}")
	}
}

fun classes()
{
	Foo(1).method1()
}

fun operators()
{
	println("arithmetic: + - * / %")
	println("assignment: =")
	println("augmented assignment: += -= *= /= %=")
	println("incremental: ++ -- (prefix or postfix)")
	println("logical: && || !")
	println("equality: == !=")
	println("referential equality: === !==")
	println("comparison: < > <= >=")
	println("index: [] (translated to getter and setters)")
	println("assertion: !! (null check)")
	println("safe call: ?. (call if non-null instance)")
	println("elvis: ?: (take right-hand val if left is null)")
	println("obj reference: :: (ref member or class)")
	println("var reference: $ (inside of a string)")
	println("reference: * @ (pass array to a vararg parm, ref this/super)")
	println("range: ..")
	println("declaration separator: : (between name and type)")
	println("statement separator: ; (multiple per line)")
	println("nullable: ? (type can be null)")
	println("lambda: -> (separate parm and body)")
	println("annotation: @ (separate parm and body)")
	println("place holder: _ (unused parm in lambda)")
}

fun precedence()
{
	println("Operator Order of Precedence:")
	println("\toper w/ higher precedence goes 1st when 2 share an operand")
	println("\tPostfix	++, --, ., ?., ?")
	println("\tPrefix	-, +, ++, --, !, labelDefinition@")
	println("\tType RHS	:, as, as?")
	println("\tMultiplicative	*, /, %")
	println("\tAdditive	+, -")
	println("\tRange	..")
	println("\tInfix function	SimpleName")
	println("\tElvis	?:")
	println("\tNamed checks	in, !in, is, !is")
	println("\tComparison	<, >, <=, >=")
	println("\tEquality	==, !==")
	println("\tConjunction	&&")
	println("\tDisjunction	||")
	println("\tAssignment	=, +=, -=, *=, /=, %=")
}

fun main(args: Array<String>)
{
	numbers()
	characters()
	arrays()
	enums()
	classes()
	operators()
	precedence()
}
