Khallware (Kotlin Proofs-of-Concept)
=================
Overview
---------------
Here are some simple, self-contained proofs-of-concept written in kotlin.

Topics include:

* [Basics](basics) - simple kotlin constructs, data_types, functional and OO programming and language features
* [Third Party](3rdParty) - maven

One-Time-Only
---------------
```shell
docker run -it --name kotlin -h kotlin centos bash
yum install -y wget git-core java-1.8.0-openjdk
wget -c 'https://github.com/JetBrains/kotlin/releases/download/v1.3.21/kotlin-native-linux-1.1.2.tar.gz' -O /tmp/kotlin.tgz
mkdir -p ~/3rdParty/kotlin && tar zxvf /tmp/kotlin.tgz --strip-components=1 -C ~/3rdParty/kotlin
export PATH=$PATH:$HOME/3rdParty/kotlin/bin/
git clone https://gitlab.com/harkwell/kotlin-poc.git && cd kotlin-poc/
```
